/**
 * @author: Salvatore Randazzo
 * @date: 13/08/2014
 * @module app 
 */

(function (app) {	
	$('.api-opener').on('click', function () {
		$('.api table').slideDown('fast');
	});
	
	$('.api table').on('click', function () {
		$(this).slideUp('fast');
	});
	
	new app.views.lessons({
		el: 'body',
		collection: new app.collections.lessons()
	});
	
}(window.app = window.app || {}));
