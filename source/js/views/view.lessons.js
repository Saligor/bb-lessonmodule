/**
 * @author: Salvatore Randazzo
 * @date: 13/08/2014
 * @module app.views 
 */

(function (app) {
	app.views = app.views || {};
	
	/**
	 *  
	 */
	app.views.lessons = Backbone.View.extend({
		/**
	 	 * @method initialize  
	 	 */
		initialize: function () { 
			this.$lessonsContainer = this.$el.find('.main');
			this.lessonViews = [];
			this.collection.fetch().done(this.render.bind(this));
		},
		/**
	 	 * @method render  
	 	 */
		render: function () {
			this.lessonViews = [];
			_.each(this.collection.models, function (lesson) {
				this.lessonViews.push(new app.views.lesson({
					model: lesson
				}));
			}, this);
			this.$lessonsContainer.html(_.pluck(this.lessonViews, '$el'));
		}
	});
	
}(window.app = window.app || {}));
