/**
 * @author: Salvatore Randazzo
 * @date: 13/08/2014
 * @module app.views 
 */

(function (app) {
	app.views = app.views || {};
	
	/**
	 * @class app.views.lessonContent
	 * @constructor
	 */
	app.views.lessonContent = Backbone.View.extend({
		/**
		 * Undescore Template
		 *
		 * @property template
		 * @type {Object}
		 */
		template: _.template($('#lessonContent-template').html()),
		/**
		 * Backbone Events
		 *
		 * @property events
		 * @type {Object}
		 */
		events: {
			'click .save': 'saveNewLessonContent'
		},
		/**
	 	 * @method initialize  
	 	 */
		initialize: function () {
			this.render();
			this.listenTo(this.model, 'change', this.render);
		},
		/**
	 	 * @method render  
	 	 */
		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
		},
		/**
	 	 * @method saveNewLessonContent  
	 	 */
		saveNewLessonContent: function () {
			// TODO: remove model Hack
			this.model.set({'description': 'Descriotion for ' + this.model.get('title') + ' has Been changed to this'});
			this.model.save();
		}
	});
	
}(window.app = window.app || {}));
