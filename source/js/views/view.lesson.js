/**
 * @author: Salvatore Randazzo
 * @date: 13/08/2014
 * @module app.views 
 */

(function (app) {
	app.views = app.views || {};
	
	/**
	 * @class app.views.lesson
	 * @constructor
	 */
	app.views.lesson = Backbone.View.extend({
		/**
		 * @property tagName
		 * @type {String}
		 * @default "section"
		 */
		tagName: 'section',
		/**
		 * Undescore Template
		 *
		 * @property template
		 * @type {Object}
		 */
		template: _.template($('#lesson-template').html()),
		/**
		 * Backbone Events
		 *
		 * @property events
		 * @type {Object}
		 */
		events: {
			'click a': 'renderLessonContent'
		},
		/**
	 	 * @method initialize  
	 	 */
		initialize: function () {
			this.lessonContentView = null;
			this.render();
			this.listenTo(this.model, 'change', this.render);
		},
		/**
	 	 * @method render  
	 	 */
		render: function () {
			this.lessonContentView = null;
			this.$el.addClass('lb ' + this.model.get('type'));
			this.$el.html(this.template(this.model.toJSON()));
		},
		/**
	 	 * @method renderLessonContent  
	 	 */
		renderLessonContent: function () {
			if (this.lessonContentView === null) {
				this.lessonContentView = new app.views.lessonContent({
					model: this.model
				});
				this.$el.append(this.lessonContentView.$el);
			}
		}
	});
	
}(window.app = window.app || {}));
