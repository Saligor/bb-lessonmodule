/**
 * @author: Salvatore Randazzo
 * @date: 13/08/2014
 * @module app.models 
 */

(function (app) {
	app.models = app.models || {};
	
	app.models.lesson = Backbone.Model.extend({
		urlRoot: 'api/v1/lessons'
	});
	
}(window.app = window.app || {}));
