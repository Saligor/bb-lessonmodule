/**
 * @author: Salvatore Randazzo
 * @date: 13/08/2014
 * @module app.collections 
 */

(function (app) {
	app.models = app.models || {};
	app.collections = app.collections || {};
	
	app.collections.lessons = Backbone.Collection.extend({
		model: app.models.lesson,
		url: 'api/v1/lessons'
	});
	
}(window.app = window.app || {}));
