<?php

require '../libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

class newLesson {
	public $id = "";
	public $title = "";
	public $description = "";
	public $type = "";
}
/**
 * Get all lessons
 */
$app->get('/lessons', function () {
	// Get all lessons
	$lessons = array();
	$con = mysqli_connect("localhost","root","","my_lessons");

	// Check connection
	if (mysqli_connect_errno()) {
	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$result = mysqli_query($con, "SELECT * FROM my_lesson");

	while($row = mysqli_fetch_array($result)) {
		$a = new newLesson();
		$a->id = $row['id'];
		$a->title = $row['title'];
		$a->description = $row['description'];
		$a->type = $row['type'];
		array_push($lessons, $a);
	}
	
	mysqli_close($con);
		
	echo json_encode($lessons);
});
$app->post('/lessons', function () use ($app) {
	// Add new Lesson
	echo $app->request()->getBody();
});
$app->get('/lessons/:id', function ($id) use ($app) {
	// Get One Specific Lesson
	echo $app->request()-getBody();
});
$app->delete('/lessons/:id', function ($id) use ($app) {
	// Delete lesson from db
	echo $app->request()->getBody();
});
$app->put('/lessons/:id', function ($id) use ($app) {
	echo $app->request()->getBody();
});

$app->run();

?>